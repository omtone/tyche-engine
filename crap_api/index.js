const path = require("path");
const express = require("express");
const app = express();
const port = 3456;
const morgan = require("morgan");

app.use(morgan("tiny"));

app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

app.get("/:file", (req, res) => {
  res.sendFile(path.join(__dirname, "./static/mp3", req.params.file));
});

app.listen(port);
