export default class VoidSample {
  constructor(config) {
    this.duration = config.duration;
    this.weight = config.weight;
  }

  getMillisecondDuration = () => this.duration * 1000;

  play = () => {};

  pause = () => {};

  stop = () => {};
}
