import { Howl } from "howler";
import getRandomInRange from "../util/getRandomInRange.js";

export default class Sample {
  constructor(config, onLoadCallback = () => {}) {
    const sampleConfig = {
      src: [ ...config.src ],
      volume: getRandomInRange(config.minVolume, config.maxVolume),
      rate: config.playbackRate,
      onload: () => {
        onLoadCallback();
        logLoadingInformation(this.audioElement._src);
      },
    };

    this.minVolume = config.minVolume;
    this.maxVolume = config.maxVolume;
    this.audioElement = new Howl(sampleConfig);
    this.fadeTimeout = config.fadeTimeout;
    this.weight = config.weight;

    function logLoadingInformation(src) {
      //  TODO: Make an API call to record which source file was loaded for system stats.
    }
  }

  isPlaying = false;
  isPaused = false;

  fadeIn = () => this.audioElement.fade(0, 1, this.fadeTimeout);

  fadeOut = () => this.audioElement.fade(1, 0, this.fadeTimeout);

  getMillisecondDuration = () => {
    return Math.floor(this.audioElement.duration() * 1000);
  };

  play = () => {
    // TODO: Wrap this in error handling
    this.audioElement.volume(getRandomInRange(this.minVolume, this.maxVolume, false));
    this.audioElement.play();
    // this.fadeIn();
    this.isPlaying = true;
    this.isPaused = false;
  };

  pause = () => {
    this.audioElement.pause();
    this.isPaused = true;
  };

  stop = () => {
    // this.fadeOut();
    this.audioElement.stop();
    this.isPlaying = false;
    this.isPaused = false;
  };
}
