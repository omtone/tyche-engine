export default class MockTrack {
  constructor(mockAvailableSamples) {
    this.availableSamples = mockAvailableSamples;
  }

  started = false;

  setAvailableSamples = mockSamples => this.availableSamples = mockSamples;

  start = () => this.started = true;

  stop = () => this.started = false;
}
