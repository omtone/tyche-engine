export default class MockSample {
  constructor(name) {
    this.name = name;
    this.length = .001;
  }

  name = null;

  length = null;

  isPlaying = false;

  isPaused = false;

  getMillisecondDuration = () => {
    return this.length;
  };

  play = () => {
    this.isPlaying = true;
    this.isPaused = false;
  };

  pause = () => {
    this.isPaused = true;
  };

  stop = () => {
    this.isPlaying = false;
  };
}
