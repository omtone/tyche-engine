import MockTrack from "./mocks/MockTrack.js";
import TycheEngine from "./TycheEngine.js";

describe("Tyche Engine", () => {
  let engine;
  const mockPlaylist = [
    {
      name: "a",
      samples: [ 1 ],
    },
    {
      name: "b",
      samples: [ 2 ],
    },
    {
      name: "c",
      samples: [ 3 ],
    },
  ];
  const tracks = mockPlaylist.reduce((acc, curr) => [ ...acc, curr.name ], []);

  beforeEach(() => {
    engine = new TycheEngine(MockTrack);
  });

  it("can be newed up.", () => {
    expect(engine).toBeInstanceOf(TycheEngine);
  });

  it("has a Track that it uses.", () => {
    expect(engine.Track).toEqual(MockTrack);
  });

  it("can be started", () => {
    engine.startPlaylist(mockPlaylist);

    tracks.forEach(track => {
      expect(engine.tracks.hasOwnProperty(track)).toEqual(true);
      expect(engine.tracks[ track ]).toBeInstanceOf(MockTrack);
      expect(engine.tracks[ track ].started).toEqual(true);
    });
    expect(engine.isRunning).toEqual(true);
  });


  it("can be stopped.", () => {
    engine.startPlaylist(mockPlaylist);
    engine.stop();

    tracks.forEach(track => {
      expect(engine.tracks[ track ].started).toEqual(false);
    });
    expect(engine.isRunning).toEqual(false);
  });

  it("can be paused.", () => {
    engine.startPlaylist(mockPlaylist);
    engine.pause();

    expect(engine.isPaused).toEqual(true);
  });

  it("can be unpaused.", () => {
    engine.startPlaylist(mockPlaylist);
    engine.pause();
    engine.unpause();

    expect(engine.isPaused).toEqual(false);
  });
});
