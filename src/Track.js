import Chance from "chance";

const chance = new Chance(Math.random);

export default class Track {
  constructor(availableSamples = []) {
    this.availableSamples = availableSamples;
  }

  currentSample = null;

  timeout = null;

  getRandomSampleIndex = () => {
    const sampleWeights = this.availableSamples.map(sample => sample.weight);

    return chance.weighted(this.availableSamples, sampleWeights);
  };

  setAvailableSamples = availableSamples => this.availableSamples = availableSamples;

  setCurrentSample = (sample) => this.currentSample = sample;

  //TODO: Handle potential errors from `Sample().play()`
  playCurrentSample = () => this.currentSample.play();

  isPlaying = () => this.currentSample.isPlaying;

  start = () => {
    try {
      if (this.currentSample && this.isPlaying()) this.currentSample.stop();

      this.setCurrentSample(this.getRandomSampleIndex());

      if (this.currentSample && this.currentSample.audioElement && this.currentSample.audioElement._duration > 0) this.playCurrentSample();

      this.timeout = setTimeout(this.start, (this.currentSample && this.currentSample.getMillisecondDuration()) || 5);
    } catch (e) {
      // TODO: Add error handling.
      console.warn(e);
    }
  };

  stop = () => {
    try {
      this.currentSample.stop();
      clearTimeout(this.timeout);
      this.timeout = null;
    } catch (e) {
      // TODO: Add error handling.
      console.warn(e);
    }
  };
}
