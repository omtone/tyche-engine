import Sample from "./samples/Sample.js";
import VoidSample from "./samples/VoidSample.js";
import generatePlayablePlaylist from "./util/generatePlayablePlaylist.js";

export function loadTracks(playlist) {
  return new Promise((resolve, reject) => {
    const sampleCount = playlist.reduce((acc, current) => {
      return acc += current.samples.filter(sample => sample.type === "sample").length;
    }, 0);
    let loadedSampleCount = 0;
    const tracks = generatePlayablePlaylist(Sample, VoidSample, playlist, incrementLoadedSampleCount);

    function incrementLoadedSampleCount() {
      loadedSampleCount += 1;

      if (isFullyLoaded) resolve(tracks);
    }

    function isFullyLoaded() {
      return loadedSampleCount === sampleCount;
    }
  });
}
