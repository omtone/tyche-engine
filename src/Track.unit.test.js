import MockSample from "./mocks/MockSample.js";
import Track from "./Track.js";

describe("Player", () => {
  let mockSamples1;
  let mockSamples2;

  beforeEach(() => {
    mockSamples1 = [
      new MockSample("a"),
      new MockSample("b"),
      new MockSample("c"),
    ];
    mockSamples2 = [
      new MockSample("d"),
      new MockSample("e"),
      new MockSample("f"),
    ];
  });

  it("exists.", () => {
    expect(new Track()).toBeInstanceOf(Track);
  });

  it("is newed up with an array of samples.", () => {
    const player = new Track(mockSamples1);

    expect(player.availableSamples).toEqual(mockSamples1);
  });

  it("can update its available samples.", () => {
    const player = new Track(mockSamples1);

    player.setAvailableSamples(mockSamples2);

    expect(player.availableSamples).toEqual(mockSamples2);
  });

  it("can set its current sample.", () => {
    const player = new Track(mockSamples1);

    player.setCurrentSample(mockSamples1[0]);

    expect(player.currentSample).toEqual(player.availableSamples[ 0 ]);
  });

  it("is null when its initialized.", () => {
    const player = new Track(mockSamples1);

    expect(player.currentSample).toEqual(null);
  });

  it("can play its current track.", () => {
    const player = new Track(mockSamples1);

    player.setCurrentSample(mockSamples1[0]);
    player.playCurrentSample();

    expect(player.availableSamples[ 0 ].isPlaying).toEqual(true);
    expect(player.isPlaying()).toEqual(true);
  });
});
