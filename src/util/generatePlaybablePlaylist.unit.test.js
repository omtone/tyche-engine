import MockSample from "../mocks/MockSample.js";
import VoidSample from "../samples/VoidSample.js";
import generatePlayablePlaylist from "./generatePlayablePlaylist.js";

describe("generateTracks", () => {
  it("exists.", () => {
    expect(typeof generatePlayablePlaylist).toEqual("function");
  });

  it("returns a playlist with a sample object when passed an API response with a single set and sample config.", () => {
    const mockData = [
      {
        name: "asdf",
        samples: [
          {
            type: "sample",
          },
        ],
      },
    ];

    expect(generatePlayablePlaylist(MockSample, VoidSample, mockData)[0].samples[0]).toBeInstanceOf(MockSample);
  });

  it("returns a playlist with a void sample object when passed an API response with a single set and a single void sample config.", () => {
    const mockData = [
      {
        name: "asdf",
        samples: [
          {
            type: "void",
          },
        ],
      },
    ];

    expect(generatePlayablePlaylist(MockSample, VoidSample, mockData)[0].samples[0]).toBeInstanceOf(VoidSample);
  });

  it("generates the number of tracks indicated on the set.", () => {
    const mockData = [
      {
        name: "asdf",
        count: 3,
        samples: [
          {
            type: "sample",
          },
        ],
      },
    ];

    expect(generatePlayablePlaylist(MockSample, VoidSample, mockData).length).toEqual(3);
  });
});
