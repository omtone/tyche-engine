export default function (min, max, isInteger = true) {
  //TODO: Wrap this in error handling
  const randomFloat = Math.random() * (max - min) + min;

  if (isInteger) return parseInt(randomFloat);

  return randomFloat;
}
