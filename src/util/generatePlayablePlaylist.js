function getSamples(currentSet, Sample, VoidSample, onLoadedCallback) {
  return currentSet.samples.map(sampleConfig => {
    switch (sampleConfig.type) {
      case "sample":
        return new Sample(sampleConfig, onLoadedCallback);

      case "void":
        return new VoidSample(sampleConfig);
    }
  });
}

export default function generatePlayablePlaylist(Sample, VoidSample, data, onLoadedCallback = () => {}) {
  return data.reduce((accumulator, currentSet) => {
    const { name, count = 1 } = currentSet;
    const samples = getSamples(currentSet, Sample, VoidSample, onLoadedCallback);
    const track = {
      name,
      samples,
    };

    for (let i = 0; i < count; i++) accumulator.push(track);

    return accumulator;
  }, []);
}
