export default class TycheEngine {
  constructor(Track) {
    this.Track = Track;
  }

  tracks = {};
  isRunning = false;
  isPaused = false;

  //TODO: Update playlists
  //TODO: gracefully move between playlists

  startPlaylist = (playlist) => {
    playlist.forEach(set => {
      const track = new this.Track(set.samples);
      this.tracks[ set.name ] = track;
      track.start();
    });
    this.isRunning = true;
  };

  stop = () => {
    Object.keys(this.tracks).forEach(trackName => {
      this.tracks[ trackName ].stop();
    });
    this.isRunning = false;
  };

  pause = () => this.isPaused = true;

  unpause = () => this.isPaused = false;
}
