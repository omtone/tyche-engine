import { sample, voidSample } from "./responseTemplates.js";

const playlist = [
  {
    //TODO: a set can generate multiple tracks.
    name: "bass pad",
    samples: [
      sample("bass_pad_1.mp3"),
    ],
  },
  {
    name: "percussion",
    samples: [
      voidSample(10),
      sample("perc_1.mp3"),
    ],
  },
  {
    name: "bells",
    samples: [
      sample("bells_1.mp3"),
    ],
  },
  {
    name: "bell pad",
    samples: [
      sample("bell_pad_1.mp3"),
      sample("bell_pad_2.mp3"),
    ],
  },
];

export default playlist;
