import { loadTracks } from "../../src/loadTracks.js";
import TycheEngine from "../../src/TycheEngine.js";
import Track from "../../src/Track.js";
import playlist from "./mockApiPlaylistResponse.js";

const app = document.getElementById("app");
const loadingIndicator = document.createElement("div");
loadingIndicator.innerText = "*** Loading ***";

app.appendChild(loadingIndicator);

const engine = new TycheEngine(Track);

loadTracks(playlist)
  .then(engine.startPlaylist)
  .then(() => app.removeChild(loadingIndicator));

console.log(engine);
window.engine = engine;
