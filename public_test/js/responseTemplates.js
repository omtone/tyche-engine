export function sample(url, weight = 1, maxVolume = 1, minVolume = 1, playbackRate = 1, fadeTimeout = 1000) {
  const baseUrl = "http://localhost:3456";

  return {
    type: "sample",
    src: [ `${baseUrl}/${url}`, ],
    weight,
    maxVolume,
    minVolume,
    playbackRate,
    fadeTimeout,
  };
}

export function voidSample(duration = 10, weight = 1) {
  return {
    type: "void",
    weight,
    duration,
  };
}
