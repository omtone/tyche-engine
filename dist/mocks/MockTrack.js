"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var MockTrack = function MockTrack(mockAvailableSamples) {
  var _this = this;

  _classCallCheck(this, MockTrack);

  this.started = false;

  this.setAvailableSamples = function (mockSamples) {
    return _this.availableSamples = mockSamples;
  };

  this.start = function () {
    return _this.started = true;
  };

  this.stop = function () {
    return _this.started = false;
  };

  this.availableSamples = mockAvailableSamples;
};

exports.default = MockTrack;