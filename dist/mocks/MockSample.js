"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var MockSample = function MockSample(name) {
  var _this = this;

  _classCallCheck(this, MockSample);

  this.name = null;
  this.length = null;
  this.isPlaying = false;
  this.isPaused = false;

  this.getMillisecondDuration = function () {
    return _this.length;
  };

  this.play = function () {
    _this.isPlaying = true;
    _this.isPaused = false;
  };

  this.pause = function () {
    _this.isPaused = true;
  };

  this.stop = function () {
    _this.isPlaying = false;
  };

  this.name = name;
  this.length = .001;
};

exports.default = MockSample;