"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var TycheEngine = function TycheEngine(Track) {
  var _this = this;

  _classCallCheck(this, TycheEngine);

  this.tracks = {};
  this.isRunning = false;
  this.isPaused = false;

  this.startPlaylist = function (playlist) {
    playlist.forEach(function (set) {
      var track = new _this.Track(set.samples);
      _this.tracks[set.name] = track;
      track.start();
    });
    _this.isRunning = true;
  };

  this.stop = function () {
    Object.keys(_this.tracks).forEach(function (trackName) {
      _this.tracks[trackName].stop();
    });
    _this.isRunning = false;
  };

  this.pause = function () {
    return _this.isPaused = true;
  };

  this.unpause = function () {
    return _this.isPaused = false;
  };

  this.Track = Track;
};

exports.default = TycheEngine;