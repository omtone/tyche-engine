"use strict";

var _MockTrack = _interopRequireDefault(require("./mocks/MockTrack.js"));

var _TycheEngine = _interopRequireDefault(require("./TycheEngine.js"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

describe("Tyche Engine", function () {
  var engine;
  var mockPlaylist = [{
    name: "a",
    samples: [1]
  }, {
    name: "b",
    samples: [2]
  }, {
    name: "c",
    samples: [3]
  }];
  var tracks = mockPlaylist.reduce(function (acc, curr) {
    return [].concat(_toConsumableArray(acc), [curr.name]);
  }, []);
  beforeEach(function () {
    engine = new _TycheEngine.default(_MockTrack.default);
  });
  it("can be newed up.", function () {
    expect(engine).toBeInstanceOf(_TycheEngine.default);
  });
  it("has a Track that it uses.", function () {
    expect(engine.Track).toEqual(_MockTrack.default);
  });
  it("can be started", function () {
    engine.startPlaylist(mockPlaylist);
    tracks.forEach(function (track) {
      expect(engine.tracks.hasOwnProperty(track)).toEqual(true);
      expect(engine.tracks[track]).toBeInstanceOf(_MockTrack.default);
      expect(engine.tracks[track].started).toEqual(true);
    });
    expect(engine.isRunning).toEqual(true);
  });
  it("can be stopped.", function () {
    engine.startPlaylist(mockPlaylist);
    engine.stop();
    tracks.forEach(function (track) {
      expect(engine.tracks[track].started).toEqual(false);
    });
    expect(engine.isRunning).toEqual(false);
  });
  it("can be paused.", function () {
    engine.startPlaylist(mockPlaylist);
    engine.pause();
    expect(engine.isPaused).toEqual(true);
  });
  it("can be unpaused.", function () {
    engine.startPlaylist(mockPlaylist);
    engine.pause();
    engine.unpause();
    expect(engine.isPaused).toEqual(false);
  });
});