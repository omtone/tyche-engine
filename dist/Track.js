"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _chance = _interopRequireDefault(require("chance"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var chance = new _chance.default(Math.random);

var Track = function Track() {
  var availableSamples = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : [];

  _classCallCheck(this, Track);

  _initialiseProps.call(this);

  this.availableSamples = availableSamples;
};

exports.default = Track;

var _initialiseProps = function _initialiseProps() {
  var _this = this;

  this.currentSample = null;
  this.timeout = null;

  this.getRandomSampleIndex = function () {
    var sampleWeights = _this.availableSamples.map(function (sample) {
      return sample.weight;
    });

    return chance.weighted(_this.availableSamples, sampleWeights);
  };

  this.setAvailableSamples = function (availableSamples) {
    return _this.availableSamples = availableSamples;
  };

  this.setCurrentSample = function (sample) {
    return _this.currentSample = sample;
  };

  this.playCurrentSample = function () {
    return _this.currentSample.play();
  };

  this.isPlaying = function () {
    return _this.currentSample.isPlaying;
  };

  this.start = function () {
    try {
      if (_this.currentSample && _this.isPlaying()) _this.currentSample.stop();

      _this.setCurrentSample(_this.getRandomSampleIndex());

      if (_this.currentSample && _this.currentSample.audioElement && _this.currentSample.audioElement._duration > 0) _this.playCurrentSample();
      _this.timeout = setTimeout(_this.start, _this.currentSample && _this.currentSample.getMillisecondDuration() || 5);
    } catch (e) {
      // TODO: Add error handling.
      console.warn(e);
    }
  };

  this.stop = function () {
    try {
      _this.currentSample.stop();

      clearTimeout(_this.timeout);
      _this.timeout = null;
    } catch (e) {
      // TODO: Add error handling.
      console.warn(e);
    }
  };
};