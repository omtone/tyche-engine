"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var VoidSample = function VoidSample(config) {
  var _this = this;

  _classCallCheck(this, VoidSample);

  this.getMillisecondDuration = function () {
    return _this.duration * 1000;
  };

  this.play = function () {};

  this.pause = function () {};

  this.stop = function () {};

  this.duration = config.duration;
  this.weight = config.weight;
};

exports.default = VoidSample;