"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _howler = require("howler");

var _getRandomInRange = _interopRequireDefault(require("../util/getRandomInRange.js"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Sample = function Sample(config) {
  var _this = this;

  var onLoadCallback = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : function () {};

  _classCallCheck(this, Sample);

  this.isPlaying = false;
  this.isPaused = false;

  this.fadeIn = function () {
    return _this.audioElement.fade(0, 1, _this.fadeTimeout);
  };

  this.fadeOut = function () {
    return _this.audioElement.fade(1, 0, _this.fadeTimeout);
  };

  this.getMillisecondDuration = function () {
    return Math.floor(_this.audioElement.duration() * 1000);
  };

  this.play = function () {
    // TODO: Wrap this in error handling
    _this.audioElement.volume((0, _getRandomInRange.default)(_this.minVolume, _this.maxVolume, false));

    _this.audioElement.play(); // this.fadeIn();


    _this.isPlaying = true;
    _this.isPaused = false;
  };

  this.pause = function () {
    _this.audioElement.pause();

    _this.isPaused = true;
  };

  this.stop = function () {
    // this.fadeOut();
    _this.audioElement.stop();

    _this.isPlaying = false;
    _this.isPaused = false;
  };

  var sampleConfig = {
    src: _toConsumableArray(config.src),
    volume: (0, _getRandomInRange.default)(config.minVolume, config.maxVolume),
    rate: config.playbackRate,
    onload: function onload() {
      onLoadCallback();
      logLoadingInformation(_this.audioElement._src);
    }
  };
  this.minVolume = config.minVolume;
  this.maxVolume = config.maxVolume;
  this.audioElement = new _howler.Howl(sampleConfig);
  this.fadeTimeout = config.fadeTimeout;
  this.weight = config.weight;

  function logLoadingInformation(src) {//  TODO: Make an API call to record which source file was loaded for system stats.
  }
};

exports.default = Sample;