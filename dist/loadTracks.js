"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.loadTracks = loadTracks;

var _Sample = _interopRequireDefault(require("./samples/Sample.js"));

var _VoidSample = _interopRequireDefault(require("./samples/VoidSample.js"));

var _generatePlayablePlaylist = _interopRequireDefault(require("./util/generatePlayablePlaylist.js"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function loadTracks(playlist) {
  return new Promise(function (resolve, reject) {
    var sampleCount = playlist.reduce(function (acc, current) {
      return acc += current.samples.filter(function (sample) {
        return sample.type === "sample";
      }).length;
    }, 0);
    var loadedSampleCount = 0;
    var tracks = (0, _generatePlayablePlaylist.default)(_Sample.default, _VoidSample.default, playlist, incrementLoadedSampleCount);

    function incrementLoadedSampleCount() {
      loadedSampleCount += 1;
      if (isFullyLoaded) resolve(tracks);
    }

    function isFullyLoaded() {
      return loadedSampleCount === sampleCount;
    }
  });
}