"use strict";

var _MockSample = _interopRequireDefault(require("../mocks/MockSample.js"));

var _VoidSample = _interopRequireDefault(require("../samples/VoidSample.js"));

var _generatePlayablePlaylist = _interopRequireDefault(require("./generatePlayablePlaylist.js"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

describe("generateTracks", function () {
  it("exists.", function () {
    expect(_typeof(_generatePlayablePlaylist.default)).toEqual("function");
  });
  it("returns a playlist with a sample object when passed an API response with a single set and sample config.", function () {
    var mockData = [{
      name: "asdf",
      samples: [{
        type: "sample"
      }]
    }];
    expect((0, _generatePlayablePlaylist.default)(_MockSample.default, _VoidSample.default, mockData)[0].samples[0]).toBeInstanceOf(_MockSample.default);
  });
  it("returns a playlist with a void sample object when passed an API response with a single set and a single void sample config.", function () {
    var mockData = [{
      name: "asdf",
      samples: [{
        type: "void"
      }]
    }];
    expect((0, _generatePlayablePlaylist.default)(_MockSample.default, _VoidSample.default, mockData)[0].samples[0]).toBeInstanceOf(_VoidSample.default);
  });
  it("generates the number of tracks indicated on the set.", function () {
    var mockData = [{
      name: "asdf",
      count: 3,
      samples: [{
        type: "sample"
      }]
    }];
    expect((0, _generatePlayablePlaylist.default)(_MockSample.default, _VoidSample.default, mockData).length).toEqual(3);
  });
});