"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = _default;

function _default(min, max) {
  var isInteger = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : true;
  //TODO: Wrap this in error handling
  var randomFloat = Math.random() * (max - min) + min;
  if (isInteger) return parseInt(randomFloat);
  return randomFloat;
}