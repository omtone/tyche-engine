"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = generatePlayablePlaylist;

function getSamples(currentSet, Sample, VoidSample, onLoadedCallback) {
  return currentSet.samples.map(function (sampleConfig) {
    switch (sampleConfig.type) {
      case "sample":
        return new Sample(sampleConfig, onLoadedCallback);

      case "void":
        return new VoidSample(sampleConfig);
    }
  });
}

function generatePlayablePlaylist(Sample, VoidSample, data) {
  var onLoadedCallback = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : function () {};
  return data.reduce(function (accumulator, currentSet) {
    var name = currentSet.name,
        _currentSet$count = currentSet.count,
        count = _currentSet$count === void 0 ? 1 : _currentSet$count;
    var samples = getSamples(currentSet, Sample, VoidSample, onLoadedCallback);
    var track = {
      name: name,
      samples: samples
    };

    for (var i = 0; i < count; i++) {
      accumulator.push(track);
    }

    return accumulator;
  }, []);
}