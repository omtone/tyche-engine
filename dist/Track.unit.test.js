"use strict";

var _MockSample = _interopRequireDefault(require("./mocks/MockSample.js"));

var _Track = _interopRequireDefault(require("./Track.js"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

describe("Player", function () {
  var mockSamples1;
  var mockSamples2;
  beforeEach(function () {
    mockSamples1 = [new _MockSample.default("a"), new _MockSample.default("b"), new _MockSample.default("c")];
    mockSamples2 = [new _MockSample.default("d"), new _MockSample.default("e"), new _MockSample.default("f")];
  });
  it("exists.", function () {
    expect(new _Track.default()).toBeInstanceOf(_Track.default);
  });
  it("is newed up with an array of samples.", function () {
    var player = new _Track.default(mockSamples1);
    expect(player.availableSamples).toEqual(mockSamples1);
  });
  it("can update its available samples.", function () {
    var player = new _Track.default(mockSamples1);
    player.setAvailableSamples(mockSamples2);
    expect(player.availableSamples).toEqual(mockSamples2);
  });
  it("can set its current sample.", function () {
    var player = new _Track.default(mockSamples1);
    player.setCurrentSample(mockSamples1[0]);
    expect(player.currentSample).toEqual(player.availableSamples[0]);
  });
  it("is null when its initialized.", function () {
    var player = new _Track.default(mockSamples1);
    expect(player.currentSample).toEqual(null);
  });
  it("can play its current track.", function () {
    var player = new _Track.default(mockSamples1);
    player.setCurrentSample(mockSamples1[0]);
    player.playCurrentSample();
    expect(player.availableSamples[0].isPlaying).toEqual(true);
    expect(player.isPlaying()).toEqual(true);
  });
});